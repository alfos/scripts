#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
 Lang:        Python
 Author:      Osetrov Alexander F.
 Email:       osetrovaf(at)gmail(dot)com
 Pkg:         Port check
 Version:     0.3.2
 Depends:     Python 3.2+
 Description: This script checks the availability of a network resource on the specified port
"""

import argparse
import socket

version = '0.3.2'
author = 'Osetrov Alexander F.'

#Port check
def isOpen(host, port, proto, timeout):
    try:
        host = socket.gethostbyname(host)
        for res in socket.getaddrinfo(host, port, type=proto):
            af, socktype, proto, canonname, sa = res
            try:
                s = socket.socket(af, socktype)
                s.settimeout(timeout)
                if proto == 6:
                    s.connect((host, port))
                data = ('a'*32).encode()
                s.sendto(data, (host, port))
                data = data.decode()
                data = s.recvfrom(1024)
                if proto == 6:
                    s.shutdown(2)
                s.close()
                return True, data[0].rstrip()
            except OSError as msg:
                return False, msg.strerror
            except OverflowError as msg:
                return False, msg
    except socket.gaierror as msg:
        return False, msg.strerror

#Parsing arguments
def parseArgs():
    argparser = argparse.ArgumentParser(description=f'Port availability check v.{version}',
                                        epilog=f'Author:\t{author}')
    argparser.set_defaults(proto=1, timeout=5, type=int)
    argparser.add_argument('-u', action='store_const', dest='proto', const=2,
                           help='Use UDP instead of TCP. The default is TCP.')
    argparser.add_argument('-t', action='store', dest='timeout', type=int,
                           help='Connection timeout. The default is 5 sec.')
    argparser.add_argument('-v', action='store_true', dest='verbose',
                           help='Detailed output')
    argparser.add_argument('address', help='IP address or FQDN')
    argparser.add_argument('port', type=int, help='Checked port number')
    return argparser.parse_args()

#Main program
if __name__ == "__main__":
    args = parseArgs()
    proto = {'1': 'TCP', '2': 'UDP'}
    check, msg = isOpen(args.address, args.port, args.proto, args.timeout)
    if check:
        if args.verbose:
            print(f'{msg.decode("latin-1")}')
        print(f'{args.address} is available on {proto[str(args.proto)]} port {args.port}')
    else:
        if msg:
            print(f'{msg}')
        print(f'{args.address} not available on {proto[str(args.proto)]} port {args.port}')

