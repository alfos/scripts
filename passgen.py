# !/usr/bin/env python
#  -*- coding: utf-8 -*-

"""
 Lang:        Python
 Author:      Osetrov Alexander F.
 Email:       osetrovaf(at)gmail(dot)com
 Pkg:         Password generator
 Version:     0.3.3
 Depends:     Python 3.9+
 Description: This script generates a secure password.
"""

import argparse
from random import choice
from itertools import chain

version = '0.3.3'
author = 'Osetrov Alexander F.'

# Generating a passwords
def passGen(length, count, symbols=['lowercase', 'uppercase']):
    passwords = []
    dictionary = {
        'lowercase': ''.join(map(chr, range(97, 123))),
        'uppercase': ''.join(map(chr, range(65, 91))),
        'digits':    ''.join(map(chr, range(48, 58))),
        'special':   ''.join(map(chr, chain(range(33, 48), range(58, 65), range(91, 97), range(123, 127))))
    }
    alphabet = ''.join([dictionary[key] for key in symbols])

    for _ in range(count):
        passwords.append(''.join(x for _ in range(length) for x in choice(alphabet)))
    
    return passwords

# Parsing arguments
def parseArgs():
    argparser = argparse.ArgumentParser(description=f'Password generator v.{version}', epilog=f'Author:\t{author}')
    argparser.add_argument('-c', '--lowercase', action='store_true', help='Includes lowercase letters in the password.')
    argparser.add_argument('-C', '--uppercase', action='store_true', help='Includes uppercase letters in the password.')
    argparser.add_argument('-d', '--digits', action='store_true', help='Includes digits in the password.')
    argparser.add_argument('-s', '--special', action='store_true', help='Includes special characters in the password.')
    argparser.add_argument('-l', action='store', default=16, type=int, help='The number of characters in the password. The default is 16.', dest='length')
    argparser.add_argument('-p', action='store', default=1, type=int, help='The number of passwords generated. The default is one.', dest='count')
    return argparser.parse_args()

# Main program
if __name__ == "__main__":
    args = parseArgs()
    alphabet = [key for key in vars(args) if type(vars(args)[key]) == bool and vars(args)[key]]
    if len(alphabet):
        passwords = passGen(args.length, args.count, symbols=alphabet)
    else:
        passwords = passGen(args.length, args.count)
    for password in passwords: print(password)
