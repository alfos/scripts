#!/bin/sh
#====================================
# Lang: sh
# Author: Osetrov Alexander F.
# Email: osetrov(at)gmail(dot)com
# Pkg: dwm status
# Version: 27.06.2020
# Depends: bash-completion alsa-utils
#          cmus curl xset xsetroot
#====================================

sep1="["
sep2="]"
timeout=1

dwm_date () {
   printf "%s%s%s" "$sep1" "$(date +"%d-%m-%Y %R")" "$sep2"
}

dwm_weather() {
   city="Yaroslavl"
   format="%t+%w"
   printf "%s%s%s" "$sep1" "$(curl -s wttr.in/$city?format=$format)" "$sep2"
}

dwm_alsa() {
   printf "%s%s%s" "$sep1" "$(amixer get Master | tail -n1 | sed -r 's/.*\[(.*)%\].*/\1/')" "$sep2"
}

dwm_cmus() {
   if pidof cmus > /dev/null; then
      status=$(cmus-remote -Q | grep '^status ' | awk '{gsub ("status ", ""); print}' | awk '{if ($1 == "playing") {print "▶"} else {print "▣"}}')
      title=$(cmus-remote -Q | grep '^tag title ' | awk '{gsub ("tag title ", ""); print}')
      artist=$(cmus-remote -Q | grep '^tag albumartist ' | awk '{gsub ("tag albumartist ", ""); print}')

      printf "%s" "$sep1"
      printf "%s%s - %s" "$status" "$artist" "$title"
      printf "%s" "$sep2"
   fi
}

dwm_layout() {
   printf "%s%s%s" "$sep1" "$(xset -q | awk 'BEGIN { a[1]="ru"; a[0]="en" } /LED/ { print a[substr($10,5,1)]; }')" "$sep2"
}

while true; do
   xsetroot -name "$(dwm_cmus)$(dwm_weather)$(dwm_alsa)$(dwm_date)$(dwm_layout)"
   sleep $timeout
done
