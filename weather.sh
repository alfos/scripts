#!/bin/sh
#===========================================
# Lang: sh
# Author: Osetrov Alexander F.
# Email: osetrovaf(at)gmail(dot)com
# Pkg: weather
# Version: 29.05.2020
# Depends: curl
# Homepage: https://gitlab.com/alfos/scripts
#===========================================

city="Yaroslavl"

printf "%s\n" "$(curl -s wttr.in/$city?format=1)"
