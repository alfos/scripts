#!/bin/sh
#===========================================
# Lang: sh
# Author: Osetrov Alexander F.
# Email: osetrovaf(at)gmail(dot)com
# Pkg: cmus status
# Version: 27.05.2020
# Depends: cmus awk grep
# Homepage: https://gitlab.com/alfos/scripts
#===========================================

if pidof cmus > /dev/null; then
   status=$(cmus-remote -Q | grep '^status ' | awk '{gsub ("status ", ""); print}' | awk '{if ($1 == "playing") {print "->"} else {print "||"}}')
   duration=$(cmus-remote -Q | grep '^duration ' | awk '{gsub ("duration ", ""); print}')
   position=$(cmus-remote -Q | grep '^position ' | awk '{gsub ("position ", ""); print}')
   title=$(cmus-remote -Q | grep '^tag title ' | awk '{gsub ("tag title ", ""); print}')
   album=$(cmus-remote -Q | grep '^tag album ' | awk '{gsub ("tag album ", ""); print}')
   artist=$(cmus-remote -Q | grep '^tag albumartist ' | awk '{gsub ("tag albumartist ", ""); print}')

   printf "%s%s - %s - %s" "$status" "$artist" "$album" "$title"
   printf " (%0d:%02d/" $((position%3600/60)) $((position%60))
   printf "%0d:%02d)\n" $((duration%3600/60)) $((duration%60))
fi
